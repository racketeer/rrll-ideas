#lang at-exp racket/base

(require "private/printing.rkt"
         scribble/srcdoc
         (for-doc racket/base
                  scribble/manual)
         "drawing.rkt"
         racket/contract/base)

(provide

 (proc-doc/names
  print-text
  (->* (number? number? string?)
       (#:color (list/c color-channel-value/c
                        color-channel-value/c
                        color-channel-value/c))
       drawable-width/c)
  ((x y text)
   ((color '(1 1 1))))
  @{

    Prints given text at specified location, optionally coloring it by
    given color triplet.

    })

 (proc-doc/names
  set-text-font
  (->* ()
       (#:family (or/c #f symbol?)
        #:weight (or/c #f number?)
        #:style (or/c #f symbol?)
        #:size (or/c #f number?))
       void?)
  (()
   ((family #f)
    (weight #f)
    (style #f)
    (size #f)))
  @{

    Sets current font for @racket[print-text] and sets up relevant
    caches accordingly.

    })

 )
