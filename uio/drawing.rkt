#lang at-exp racket

(require "private/drawing.rkt"
         scribble/srcdoc
         (for-doc racket/base
                  scribble/manual)
         "private/textures-structs.rkt")

(define color-channel-value/c
  (and/c number?
         (>=/c 0)
         (<=/c 1)))

(define drawable-width/c
  (and/c number?
         (not/c negative?)))

(provide

 (proc-doc/names
  color-channel-value/c
  (-> any/c boolean?)
  (v)
  @{

    Returns @racket[#t] for any numeric value between @racket[0] and
    @racket[1] (inclusive).

    })

 (proc-doc/names
  drawable-width/c
  (-> any/c boolean?)
  (v)
  @{

    A contract matching any non-negative number.

    })

 (form-doc
  (uio-draw body ...)
  @{

    Runs enclosed body with OpenGL context and swaps GL buffers
    afterwards.

    })

 (proc-doc/names
  draw-tile
  (-> symbol?
      number? number?
      drawable-width/c drawable-width/c
      void?)
  (texture-sym x0 y0 w h)
  @{

    Draws given texture at given location scaling it to requested
    width and height.

    })

 (proc-doc/names
  draw-lit-tile
  (->* (symbol?
        number? number?
        drawable-width/c drawable-width/c)
       (color-channel-value/c
        color-channel-value/c
        color-channel-value/c
        color-channel-value/c
        color-channel-value/c)
       void?)
  ((texture-sym x0 y0 w h)
   ((tl 1)
    (tr 1)
    (c 1)
    (bl 1)
    (br 1)))
  @{

    Draws given texture at given location scaling it to reqeusted
    width and height applying given light at the four corners and center
    of the tile.

    })

 (proc-doc/names
  draw-sprite
  (->* ((or/c sprite? symbol?)
        number? number?)
       (#:color (list/c color-channel-value/c
                        color-channel-value/c
                        color-channel-value/c))
       void?)
  ((sprite/symbol x0 y0)
   ((color #f)))
  @{

    Draws given sprite at specified location using its width and
    height optionally coloring it using provided color triplet.

    })

 (proc-doc/names
  clear-screen
  (->* ()
       (number? number? number?)
       void?)
  (()
   ((r 0)
    (g 0)
    (b 0)))
  @{

    Clears the color buffer of screen GL context using provided color
    triplet.

    })

 (proc-doc/names
  set-clip-rectangle
  (-> number? number? number? number? void?)
  (x0 y0 x1 y1)
  @{

    Sets clip planes 0-3 to limit the drawing viewport.

    })

 (proc-doc
  reset-clip-rectangle
  (-> void?)
  @{

    Disables clipping planes 0-3, effectively removing clipping
    rectangle.

    })

 (form-doc
  (with-viewport x0 y0 w h ox oy body ...)
  @{

    Runs given @racket[body] with viewport starting at x0, y0 of w, h
    width and height respectively, translating origin at ox, oy.

    })

 )
