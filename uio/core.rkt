#lang at-exp racket/base

(require "private/core.rkt"
         scribble/srcdoc
         (for-doc racket/base
                  scribble/manual)
         racket/contract/base)

(provide

 (form-doc
  (with-uio maybe-options expression ...)
  #:grammar ((maybe-options (code:line maybe-option ...))
             (maybe-option (code:line)
                           (code:line #:width width)
                           (code:line #:height height)
                           (code:line #:scale scale)
                           (code:line #:max-textures max-textures)
                           (code:line #:label label)
                           (code:line #:fullscreen)))
  #:contracts ((width exact-positive-integer?)
               (height exact-positive-integer?)
               (scale exact-positive-integer?)
               (max-textures exact-positive-integer?)
               (label string?))
  @{

    Runs enclosed block with associated GUI frame (window) and GL
    context. All UI operations must be used within this block.

    Default values are:

    @itemlist[
              @item{width = @racket[1280]}
              @item{height = @racket[768]}
              @item{scale = @racket[1]}
              @item{max-textures = @racket[1024]}
              @item{label = @racket["RRLL UI"]}
              ]

    })

 (proc-doc
  uio-width
  (-> exact-positive-integer?)
  @{

    Returns the screen width.

    })

 (proc-doc
  uio-height
  (-> exact-positive-integer?)
  @{

    Returns the screen height.

    })
    

 )
