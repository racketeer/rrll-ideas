#lang racket/base

(require racket/flonum
         racket/fixnum
         "generics.rkt"
         "vec2.rkt")

(provide dda)

(define (fx/fl->fl n (fx-adjust 0.0))
  (if (flonum? n)
      n
      (fl+ (fx->fl n) fx-adjust)))

(define (dda rlg arg-start-x arg-start-y arg-rv-x arg-rv-y
             (arg-limit-t +inf.0) (wall? (λ (v) (not v)))
             (skip-start-tile #t))
  (define limit-t (fx/fl->fl arg-limit-t))
  ; integers -> 0.5,0.5 ; flonums => whatever gets passed
  (define r0-x (fx/fl->fl arg-start-x 0.5))
  (define r0-y (fx/fl->fl arg-start-y 0.5))
  (define rv-x (fx/fl->fl arg-rv-x 0.5))
  (define rv-y (fx/fl->fl arg-rv-y 0.5))
  
  ; Get current tile top-left corner
  (define Tx (flfloor r0-x))
  (define Ty (flfloor r0-y))
  
  ; Get first horizontal crossing
  (define H0y (if (fl> rv-y 0.0) (fl+ Ty 1.0) Ty))
  (define ΔH0y (flabs (fl- H0y r0-y))) ; distance to 1st horizontal crosssing

  ; Get ray displacement between two horizontal lines
  (define ΔHt (flabs (fl/ 1.0 rv-y)))
  (define ΔHMy (if (fl> rv-y 0.0) 1 -1)) ; map index displacement
  (define H0d (fl* ΔH0y ΔHt)) ; ray multiplier

  ; Get first vertical crossing
  (define V0x (if (fl> rv-x 0.0) (fl+ Tx 1.0) Tx))
  (define ΔV0x (flabs (fl- V0x r0-x)))
  
  ; Get ray displacement between two vertical lines
  (define ΔVt (flabs (fl/ 1.0 rv-x)))
  (define ΔVMx (if (fl> rv-x 0.0) 1 -1)) ; map index displacement
  (define V0d (fl* ΔV0x ΔVt)) ; ray multiplier

  (define Mx0 (fl->fx Tx))
  (define My0 (fl->fx Ty))

  ; DDA loop
  (let ray-loop ((Ht (fl+ H0d))
                 (Vt (fl+ V0d))
                 (Mx Mx0)
                 (My My0)
                 (Lt (fl+ 0.0)))
    (cond
      ((fl>= Lt limit-t)
       (values #f #f #f))
      ((and (wall? (rlgrid-ref rlg Mx My))
            (or (not skip-start-tile)
                (not (and (eq? Mx Mx0)
                          (eq? My My0)))))
       (values Lt (rlgrid-ref rlg Mx My) (vec2 Mx My)))
      (else
       (if (fl< Vt Ht)
           (ray-loop Ht
                     (fl+ Vt ΔVt)
                     (fx+ Mx ΔVMx)
                     My
                     Vt)
           (ray-loop (fl+ Ht ΔHt)
                     Vt
                     Mx
                     (fx+ My ΔHMy)
                     Ht))))))
