#lang scribble/manual

@(require (only-in scribble/core make-style)
          (only-in scribble/latex-properties latex-defaults+replacements))

@(define my-style
   (make-style
    #f
    (list (latex-defaults+replacements
           (list 'collects #"scribble" #"scribble-prefix.tex")
           (list 'collects #"scribble" #"scribble-style.tex")
           '()
           (hash "scribble-load-replace.tex" #"\\renewcommand{\\packageMathabx}{\\relax}\n")))))

@require[scribble/extract pict scribble-math/dollar]

@title[#:style my-style]{Rogue-like User Input/Output}

@section{Core}
@defmodule[uio/core]

This module provides the core UI wrapper form.

@include-extracted["../uio/core.rkt"]

@section{Drawing}
@defmodule[uio/drawing]
@include-extracted["../uio/drawing.rkt"]

@section{Input}
@defmodule[uio/input]
@include-extracted["../uio/input.rkt"]

@section{Printing}
@defmodule[uio/printing]
@include-extracted["../uio/printing.rkt"]

@section{Textures}
@defmodule[uio/textures]
@include-extracted["../uio/textures.rkt"]
