#lang scribble/manual

@(require (only-in scribble/core make-style)
          (only-in scribble/latex-properties latex-defaults+replacements))

@(define my-style
   (make-style
    #f
    (list (latex-defaults+replacements
           (list 'collects #"scribble" #"scribble-prefix.tex")
           (list 'collects #"scribble" #"scribble-style.tex")
           '()
           (hash "scribble-load-replace.tex" #"\\renewcommand{\\packageMathabx}{\\relax}\n")))))

@require[scribble/extract pict scribble-math/dollar]

@title[#:style my-style]{Rogue-Like Grids}

These modules provide the purely functional immutable grid
implementations. The main differences are memory consumption and the
time and memory complexity of referencing or setting a tile to a new
value.

@section{Generics}
@defmodule[rlgrid/generics]

The generic interface deals only with accessing and modifying the
grid. On top of this interface more common functionality is added.

@defthing[gen:rlgrid any/c]{

  A generic interface that provides access methods to tile of an
  underlying grid. The following methods must be implemented:

  @itemlist[
	@item{@racket[(rlgrid-ref grid x y)] - retrieves tile at given coordinates}
	@item{@racket[(rlgrid-set grid x y value)] - sets tile at given coordinates to a new value}
	@item{@racket[(rlgrid-width grid)] - returns the width of the grid}
	@item{@racket[(rlgrid-height grid)] - returns the height of the grid}
	]

  The structs implementing this generic interface can - and probably
  will - implement other means of inspection. Construction of the
  underlying data structures is not covered by this generic interface.
  
}

@include-extracted["../rlgrid/generics.rkt"]

@section{tgrid: Tree-Based Grid}
@defmodule[rlgrid/tgrid]

@itemlist[
	@item{@${n=w\cdot h}}
	]
	
  @tabular[#:style 'boxed
  	   #:column-properties '(left center center)
	   #:row-properties '(bottom-border ())
	   #:sep @hspace[1]
	   (list (list "Method" "Time" "Memory")
	   	 (list "Make" @${\Theta(n)} @${\Theta(n)})
		 (list "Ref" @${\mathcal{O}(\log_2n)} @${\Theta(1)})
		 (list "Set" @${\mathcal{O}(\log_2n)} @${\mathcal{O}(1)}))
	]

This module implements @racket[gen:rlgrid] that stores the grid in a
binary tree structure. At each level of splitting the longer dimension
is split in half with width taking precedence if they are the
same. The full tree is always allocated upon construction.

A 4x3 grid tree internally looks like:

@(define (make-grid-pict w h (x0 0) (y0 0) (wm #f) (s 20))
  (define rwm (if wm wm w))
  (apply vc-append (for/list ((j (in-range h)))
    (apply hc-append (for/list ((i (in-range w)))
      (cc-superimpose
        (rectangle s s)
        (text (format "~a" (+ (* rwm (+ j y0)) i x0)))
        ))))))

@(vc-append
  (make-grid-pict 4 3)
  (blank 10 10)
  (hc-append
   (make-grid-pict 2 3 0 0 4)
   (blank 120 10)
   (make-grid-pict 2 3 2 0 4))
  (blank 10 10)
  (hc-append
   (make-grid-pict 2 1 0 0 4)
   (blank 40 10)
   (make-grid-pict 2 2 0 1 4)
   (blank 80 10)
   (make-grid-pict 2 1 2 0 4)
   (blank 40 10)
   (make-grid-pict 2 2 2 1 4)
   )
  (blank 10 10)
  (hc-append
   (make-grid-pict 1 1 0 0 4)
   (blank 10 10)
   (make-grid-pict 1 1 1 0 4)
   (blank 20 10)
   (make-grid-pict 2 1 0 1 4)
   (blank 10 10)
   (make-grid-pict 2 1 0 2 4)
   (blank 40 10)
   (make-grid-pict 1 1 2 0 4)
   (blank 10 10)
   (make-grid-pict 1 1 3 0 4)
   (blank 20 10)
   (make-grid-pict 2 1 2 1 4)
   (blank 10 10)
   (make-grid-pict 2 1 2 2 4)
   )
   (blank 10 10)
   (hc-append
    (blank 80 10)
    (make-grid-pict 1 1 0 1 4)
    (blank 10 10)
    (make-grid-pict 1 1 1 1 4)
    (blank 20 10)
    (make-grid-pict 1 1 0 2 4)
    (blank 10 10)
    (make-grid-pict 1 1 1 2 4)
    (blank 80 10)
    (make-grid-pict 1 1 2 1 4)
    (blank 10 10)
    (make-grid-pict 1 1 3 1 4)
    (blank 20 10)
    (make-grid-pict 1 1 2 2 4)
    (blank 10 10)
    (make-grid-pict 1 1 3 2 4)
   )
  )

@include-extracted["../rlgrid/tgrid.rkt"]

@section{stgrid: Sparse Tree-Based Grid}
@defmodule[rlgrid/stgrid]

@itemlist[
	@item{@${n=w\cdot h}}
	]
	
  @tabular[#:style 'boxed
  	   #:column-properties '(left center center)
	   #:row-properties '(bottom-border ())
	   #:sep @hspace[1]
	   (list (list "Method" "Time" "Memory")
	   	 (list "Make" @${\Theta(1)} @${\Theta(1)})
		 (list "Ref" @${\mathcal{O}(\log_2n)} @${\Theta(1)})
		 (list "Set" @${\mathcal{O}(\log_2n)} @${\mathcal{O}(1)}))
	]

This module implements @racket[gen:rlgrid] that stores the grid in a
sparse binary tree where all the tiles containing values
@racket[equal?] to the @racket[default] given are not allocated and if
both child nodes of given node contain unallocated values, it is also
left unallocated and the whole sub-tree is not created.

@include-extracted["../rlgrid/stgrid.rkt"]

@section{dtgrid: Deduplicated Tree Based Grid}
@defmodule[rlgrid/dtgrid]

This module implements @racket[gen:rlgrid] that stores the grid in a
binary tree where any node can be collapsed if its two child nodes
contain values that are @racket[equal?] to each other of if the child
nodes are both collapsed and their collapsed value is the same.

@include-extracted["../rlgrid/dtgrid.rkt"]

@section{vgrid: Vector-Based Grid}
@defmodule[rlgrid/vgrid]

@itemlist[
	@item{@${n=w\cdot h}}
	]
	
  @tabular[#:style 'boxed
  	   #:column-properties '(left center center)
	   #:row-properties '(bottom-border ())
	   #:sep @hspace[1]
	   (list (list "Method" "Time" "Memory")
	   	 (list "Make" @${\Theta(n)} @${\Theta(n)})
		 (list "Ref" @${\Theta(1)} @${\Theta(1)})
		 (list "Set" @${\Theta(n)} @${\Theta(n)}))
	]

This module implements @racket[gen:rlgrid] that stores the grid in a
one-dimensional @racket[vector]. The tiles in a row are stored from
left to right and the rows are concatenated from top to bottom
linearly in the vector.

@include-extracted["../rlgrid/vgrid.rkt"]

@section{2D Integer Vectors}
@defmodule[rlgrid/vec2]

A convenience structure for representing 2D coordinates in the
grid. The @racket[x] and @racket[y] values can only be
@racket[fixnum?]s.

@include-extracted["../rlgrid/vec2.rkt"]

@section{Utilities}
@defmodule[rlgrid/utils]

This module contains simple utilities commonly used by various
algorithms in other modules.

@include-extracted["../rlgrid/utils.rkt"]

@section{Discrete Differential Analysis}
@defmodule[rlgrid/dda]
@include-extracted["../rlgrid/dda.rkt"]

@section{Determining Visibility}
@defmodule[rlgrid/visibility]


@include-extracted["../rlgrid/visibility.rkt"]
