#lang scribble/manual

@(require (only-in scribble/core make-style)
          (only-in scribble/latex-properties latex-defaults+replacements))

@(define my-style
   (make-style
    #f
    (list (latex-defaults+replacements
           (list 'collects #"scribble" #"scribble-prefix.tex")
           (list 'collects #"scribble" #"scribble-style.tex")
           '()
           (hash "scribble-load-replace.tex" #"\\renewcommand{\\packageMathabx}{\\relax}\n")))))

@title[#:style my-style]{Racket Rogue-Like Library}
@author[
 @author+email["Dominik Pantůček" "dominik.pantucek@trustica.cz"]]

@(table-of-contents)

@section{Introduction}

This library provides the building primitives for writing roguelike
games.

@include-section["rlgrid.scrbl"]

@include-section["uio.scrbl"]
